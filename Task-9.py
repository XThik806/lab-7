# Описати рекурсивну функцію Palindrome (S) логічного типу, яка повертає True,
# якщо ціле S є паліндромом (Читається однаково зліва направо і справа наліво) і False в іншому випадку.
# Оператор циклу в тілі функції не використовувати.
# Вивести значення функції Palindrome для п'яти даних чисел.


def Palindrome(S):
    if len(S) <= 1:
        return True
    if S[0] != S[-1]:
        return False
    return Palindrome(S[1:-1])

for i in range(5):
    str = input(f"Введіть {i+1}-й текст: ").lower()
    if Palindrome(str): print("Цей текст є палнідромом")
    else: print("Це звичайний текст, не паліндром")