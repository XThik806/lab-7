# Описати рекурсивну функцію GCD (A, B) цілого типу, яка знаходить найбільший 
# спільний дільник (GCD, greatest common divisor) двох цілих позитивних чисел A і B, використовуючи алгоритм Евкліда:
# GCD (A, B) = GCD (B, A mod B), B ≠ 0; GCD (A, 0) = A,
# де «mod» позначає операцію взяття залишку від ділення. 
# За допомогою цієї функції знайти GCD (A, B), GCD (A, C), GCD (A, D), якщо дано числа A, B, C, D.

def GCD(A, B):
    if B == 0:
        return A
    else:
        return GCD(B, A % B)
    

a = int(input(f"Введи значення A: "))

for i in range(3):
    b = int(input(f"Введи {i+1}-е число: "))
    print(f"GCD({a}, {b}) = {GCD(a, b)}")