# Описати рекурсивну функцію Combin1 (N, K) цілого типу, яка знаходить C (N, K) - число поєднань з N елементів по K - за допомогою рекурентного співвідношення:
# C(N,0)=C(N,N)=1, C(N,K)=C(N-1,K)+C(N-1,K-1)при0<K<N.
# Параметри функції - цілі числа; N> 0, 0 ≤ K ≤ N. Дано число N і п'ять різних значень K.
# Вивести числа C (N, K) разом з кількістю рекурсивних викликів функції Combin1, що необхідні для їх знаходження.

def Combin1(N, K):
    # global calls
    # calls += 1
    if K == 0 or K == N:
        return 1
    return Combin1(N-1, K) + Combin1(N-1, K-1)


n = int(input("Введіть значення N: "))
for i in range(5):
    k=int(input(f"Введіть {i+1} значення для K: "))
    # calls = 0
    result = Combin1(n, k)
    # print(f"C({n}, {k}) = {result}, calls = {calls}")
    print(f"C({n}, {k}) = {result}")